### Hi, I'm Alexis Sánchez :computer: 
### Currently working on JavaScript developer :1st_place_medal::desktop_computer::moneybag:

![profileImage](https://user-images.githubusercontent.com/13206614/107162701-687f5000-697b-11eb-85fe-ee630f1e2555.png)


About my attributes as a developer:

```js
const Me = () => {
  code: [Node, JavaScript, TypeScript, Solidity],
  tools: [React, React Native, React Query, GraphQL, Expo, SailsJS, Express, NestJS, NextJS, Redux, Redux Saga, Redux Toolkit, styled-components, hooks],
  git: [git, gitlab, github, Bitbucket],
  db: [Redis, MongoDB, MariaDB, MySQL, Postgresql],
  devops: [CI/CD, GitLab CI, Github Action],
  managementTool: [Azure DevOps, Asana, Trello, Jira],
  infrastructures: [AWS, Azure, DigitalOcean],
  operatingSystem: [Mac, Linux],
  workExperience: [Crizz Digital Agency, Seeconds, Sequal, Anna Group],
  projects: [YoVoto, Adkiero, Sublime, Aloha, Meixter, KoquiApp, Anna, Bric, Gurdjief, EVA]
}
```
You can find me: :world_map:
- [WS: +584120363191](https://wa.me/+584120363191)
- :briefcase: [Linkedin](https://www.linkedin.com/in/alexis-sanchez-73086211a/)
- :e-mail: [aasvdj@gmai.com](aasvdj@gmail.com)
